package de.servbuis.ranks;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import de.servbuis.ranks.commands.Demote;
import de.servbuis.ranks.commands.Promote;
import de.servbuis.ranks.listeners.PlayerChatListener;
import de.servbuis.ranks.listeners.PlayerLoginListener;

public class Ranks extends JavaPlugin {

	
	@Override
	public void onEnable() {
		
		Bukkit.getPluginManager().registerEvents(new PlayerChatListener(this), this);
		Bukkit.getPluginManager().registerEvents(new PlayerLoginListener(this), this);
		Bukkit.getPluginCommand("promote").setExecutor(new Promote(this));
		Bukkit.getPluginCommand("demote").setExecutor(new Demote(this));
		System.out.println("[Ranks] ist bereit.");
	}
}
