package de.servbuis.ranks.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.servbuis.ranks.Ranks;
import net.md_5.bungee.api.ChatColor;

public class Promote implements CommandExecutor {

	private Ranks plugin;
	public Promote(Ranks plugin) {
		this.plugin = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {

		Player p = null;
		if(sender instanceof Player)
			p = (Player) sender;
		
		if(p != null){
			if(!p.hasPermission("servbuis.admin")){
				return true;
			}
		}
		
		if(args.length != 1){
			if(p == null){
				System.out.println("Bitte gebe einen Spieler an /promote <Spieler>");
			} else {
				p.sendMessage(ChatColor.RED + "Bitte gebe einen Spieler an /promote <Spieler>");
			}
			return true;
		}
		
		Player target = Bukkit.getPlayer(args[0]);
		if(!target.isOnline()){
			if(p == null){
				System.out.println("Der Spieler ist leider nicht online.");
			} else {
				p.sendMessage(ChatColor.RED + "Der Spieler ist leider nicht online.");
			}
			return true;
		}
		int targetRank = plugin.getConfig().getInt(target.getUniqueId().toString());
		targetRank++;
		if(targetRank > 8){
		if(p == null){
		    System.out.println("Dieser Spieler kann nicht mehr promotet werden");
		    return true;
		} else {
		    p.sendMessage(ChatColor.RED + "Dieser Spieler kann nicht mehr promotet werden");
		    return true;
		}
		plugin.getConfig().set(target.getUniqueId().toString(), targetRank);
		plugin.saveConfig();
		
		return true;
	}

}
