package de.servbuis.ranks.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

import de.servbuis.ranks.Ranks;

public class PlayerLoginListener implements Listener {

	private Ranks plugin;
	
	public PlayerLoginListener(Ranks plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onLogin(PlayerLoginEvent event){
		
		Player player = event.getPlayer();
		
		if(!player.hasPlayedBefore()){
			plugin.getConfig().set(player.getUniqueId().toString(), 1);;
		}
		
	}
}
