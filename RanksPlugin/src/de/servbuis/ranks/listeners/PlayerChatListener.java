package de.servbuis.ranks.listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatEvent;

import de.servbuis.ranks.Ranks;

public class PlayerChatListener implements Listener {

	private Ranks plugin;
	
	public PlayerChatListener(Ranks plugin) {
		this.plugin = plugin;
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onChat(PlayerChatEvent event){
		
		Player player = event.getPlayer();
		String msg = event.getMessage();
		int rank = plugin.getConfig().getInt(player.getUniqueId().toString());
		event.setCancelled(true);
		
		if(rank == 1){
			Bukkit.broadcastMessage(ChatColor.GRAY + "[" + ChatColor.GREEN + "Spieler" + ChatColor.GRAY + "]" + ChatColor.WHITE + ">> " + msg);
		}
		
		if(rank == 2){
			Bukkit.broadcastMessage(ChatColor.GRAY + "[" + ChatColor.GOLD + "VIP" + ChatColor.GRAY + "]" + ChatColor.WHITE + ">> " + msg);
		}
		
		if(rank == 3){
			Bukkit.broadcastMessage(ChatColor.GRAY + "[" + ChatColor.DARK_PURPLE + "YouTuber" + ChatColor.GRAY + "]" + ChatColor.WHITE + ">> " + msg);
		}
		
		if(rank == 4){
			Bukkit.broadcastMessage(ChatColor.GRAY + "[" + ChatColor.AQUA + "Supporter" + ChatColor.GRAY + "]" + ChatColor.WHITE + ">> " + msg);
		}
		
		if(rank == 5){
			Bukkit.broadcastMessage(ChatColor.GRAY + "[" + ChatColor.DARK_AQUA + "Architekt" + ChatColor.GRAY + "]" + ChatColor.WHITE + ">> " + msg);
		}
		
		if(rank == 6){
			Bukkit.broadcastMessage(ChatColor.GRAY + "[" + ChatColor.LIGHT_PURPLE + "Moderator" + ChatColor.GRAY + "]" + ChatColor.WHITE + ">> " + msg);
		}
		
		if(rank == 7){
			Bukkit.broadcastMessage(ChatColor.GRAY + "[" + ChatColor.RED + "Admin" + ChatColor.GRAY + "]" + ChatColor.WHITE + ">> " + msg);
		}
		
		if(rank == 8){
			Bukkit.broadcastMessage(ChatColor.GRAY + "[" + ChatColor.DARK_RED + "Inhaber" + ChatColor.GRAY + "]" + ChatColor.WHITE + ">> " + msg);
		}
		
	}
}
